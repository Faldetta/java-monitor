package keyParameters;

import record.Record;

/**
 * {@code Enum} implementing the {@link Parameter} interface to be used as key
 * by the {@link Record}.
 */
public enum ExecutionTimeParameter implements Parameter {
	/**
	 * Key to identify an interval execution time record 
	 */
	INTERVAL_EXECUTION_TIME, 
	/**
	 * Key to identify a total execution time record
	 */
	TOTAL_EXECUTION_TIME
}
