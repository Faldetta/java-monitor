package keyParameters;

import record.Record;

/**
 * {@code Enum} implementing the {@link Parameter} interface to be used as key
 * by the {@link Record}.
 */
public enum ThreadInfoParameter implements Parameter {
	
	/**
	 * Key used to identify a thread count record
	 */
	THREAD_COUNT, 
	
	/**
	 * Key used to identify a thread time spent in CPU mode record
	 */
	THREAD_CPU_TIME, 
	
	/**
	 * Key used to identify a thread time spent in user mode record
	 */
	THREAD_USER_TIME, 

	/**
	 * Key used to identify a thread count variation (over a time interval) record
	 */
	THREAD_COUNT_VARIATION, 

	/**
	 * Key used to identify a thread time spent in CPU variation (over a time interval) record
	 */
	THREAD_CPU_TIME_VARIATION,

	/**
	 * Key used to identify a thread time spent in user mode variation (over a time interval) record
	 */
	THREAD_USER_TIME_VARIATION
}
