package keyParameters;

/**
 * Empty interface used for the advantages on the inheritance for the
 * {@code Enum} hierarchy contained in the {@code keyParameters} package.
 */
public interface Parameter {

}
