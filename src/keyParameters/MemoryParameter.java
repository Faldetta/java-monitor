package keyParameters;

import record.Record;

/**
 * {@code Enum} implementing the {@link Parameter} interface to be used as key
 * by the {@link Record}.
 */
public enum MemoryParameter implements Parameter {
	
	/**
	 * Key used to identify a memory record
	 */
	MEMORY, 
	
	/**
	 * Key used to identify a memory variation (over a time interval) record 	 
	 */
	MEMORY_VARIATION
}
