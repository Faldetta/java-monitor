package keyParameters;

import record.Record;

/**
 * {@code Enum} implementing the {@link Parameter} interface to be used as key
 * by the {@link Record}.
 */
public enum LoadedClassesParameter implements Parameter {
	/**
	 * Key used to identify a loaded classes record
	 */
	LOADED_CLASSES, 
	/**
	 * Key used to identify an unloaded classes record
	 */
	UNLOADED_CLASSES, 
	
	/**
	 * Key used to identify a loaded classes variation (over a time interval) record
	 */
	LOADED_CLASSES_VARIATION, 
	
	/**
	 * Key used to identify an unloaded classes variation (over a time interval) record
	 */
	UNLOADED_CLASSES_VARIATION
}
