package keyParameters;

import record.Record;

/**
 * {@code Enum} implementing the {@link Parameter} interface to be used as key
 * by the {@link Record}.
 */
public enum CompilationTimeParameter implements Parameter {
	/**
	 * Key to identify a compilation time record
	 */
	COMPILATION_TIME, 
	
	/**
	 * Key to identify a compilation time variation record 
	 */
	COMPILATION_TIME_VARIATION
}
