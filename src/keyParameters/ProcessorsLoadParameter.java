package keyParameters;

import record.Record;

/**
 * {@code Enum} implementing the {@link Parameter} interface to be used as key
 * by the {@link Record}.
 */
public enum ProcessorsLoadParameter implements Parameter {
	
	/**
	 * Key used to identify a process CPU load record
	 */
	PROCESS_LOAD, 
	
	/**
	 * Key used to identify a system CPU load record
	 */
	SYSTEM_LOAD, 
	
	/**
	 * Key used to identify a CPU time on the current process record
	 */
	PROCESS_TIME, 
	
	/**
	 * Key used to identify a system CPU average load record
	 */
	SYSTEM_LOAD_AVERAGE, 
	
	/**
	 * Key used to identify a process CPU load variation (over a time interval) record
	 */
	PROCESS_LOAD_VARIATION, 

	/**
	 * Key used to identify a system CPU load variation (over a time interval) record
	 */
	SYSTEM_LOAD_VARIATION,

	/**
	 * Key used to identify a CPU time on the current process variation (over a time interval) record
	 */
	PROCESS_TIME_VARIATION, 

	/**
	 * Key used to identify a system CPU average load variation (over a time interval) record
	 */
	SYSTEM_LOAD_AVERAGE_VARIATION
}
