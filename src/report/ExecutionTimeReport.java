package report;

import keyParameters.Parameter;
import record.ExecutionTimeRecord;
import record.Record;

/**
 * This class extend {@link Report} to model a report of an execution time
 * analysis.
 */
public class ExecutionTimeReport extends Report {

	/**
	 * Construct {@link ExecutionTimeReport}
	 */
	public ExecutionTimeReport() {
		super();
	}

	/**
	 * Extend {@link Report#add(Record)} to force the parameter type.
	 * 
	 * @param record Try to add a {@link Record} to the internal data structure.
	 * @return {@code true} if {@code record} is correctly inserted, {@code false}
	 *         otherwise (if there is already a {@link Record} with the same key
	 *         {@link Parameter}).
	 */
	public boolean add(ExecutionTimeRecord record) {
		return super.add(record);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ExecutionTimeRecord getRecord(Parameter key) {
		Record record;
		record = super.getRecord(key);
		if (record != null) {
			return (ExecutionTimeRecord) record;
		}
		return null;
	}
}
