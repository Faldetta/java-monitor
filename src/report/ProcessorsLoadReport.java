package report;

import keyParameters.Parameter;
import record.ProcessorsLoadRecord;
import record.Record;

/**
 * This class extend {@link Report} to model a report about processor usage.
 */
public class ProcessorsLoadReport extends Report {

	/**
	 * Construct a {@link ProcessorsLoadReport}
	 */
	public ProcessorsLoadReport() {
		super();
	}

	/**
	 * Extend {@link Report#add(Record)} to force the parameter type.
	 * 
	 * @param record Try to add a {@link Record} to the internal data structure.
	 * @return {@code true} if {@code record} is correctly inserted, {@code false}
	 *         otherwise (if there is already a {@link Record} with the same key
	 *         {@link Parameter}).
	 */
	public boolean add(ProcessorsLoadRecord record) {
		return super.add(record);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ProcessorsLoadRecord getRecord(Parameter key) {
		Record record;
		record = super.getRecord(key);
		if (record != null) {
			return (ProcessorsLoadRecord) record;
		}
		return null;
	}

}
