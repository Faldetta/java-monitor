package report;

import keyParameters.Parameter;
import record.LoadedClassesRecord;
import record.Record;

/**
 * This class extend {@link Report} to model a report on the number of
 * (un)loaded classes.
 */
public class LoadedClassesReport extends Report {

	/**
	 * Construct a {@link MemoryConsumptionReport}
	 */
	public LoadedClassesReport() {
		super();
	}

	/**
	 * Extend {@link Report#add(Record)} to force the parameter type.
	 * 
	 * @param record Try to add a {@link Record} to the internal data structure.
	 * @return {@code true} if {@code record} is correctly inserted, {@code false}
	 *         otherwise (if there is already a {@link Record} with the same key
	 *         {@link Parameter}).
	 */
	public boolean add(LoadedClassesRecord record) {
		return super.add(record);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public LoadedClassesRecord getRecord(Parameter key) {
		Record record;
		record = super.getRecord(key);
		if (record != null) {
			return (LoadedClassesRecord) record;
		}
		return null;
	}
}
