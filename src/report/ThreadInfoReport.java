package report;

import keyParameters.Parameter;
import record.Record;
import record.ThreadInfoRecord;

/**
 * This class extend {@link Report} to model a report about a given thread.
 */
public class ThreadInfoReport extends Report {

	/**
	 * Construct a {@link ThreadInfoReport}
	 */
	public ThreadInfoReport() {
		super();
	}

	/**
	 * Extend {@link Report#add(Record)} to force the parameter type.
	 * 
	 * @param record Try to add a {@link Record} to the internal data structure.
	 * @return {@code true} if {@code record} is correctly inserted, {@code false}
	 *         otherwise (if there is already a {@link Record} with the same key
	 *         {@link Parameter}).
	 */
	public boolean add(ThreadInfoRecord record) {
		return super.add(record);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ThreadInfoRecord getRecord(Parameter key) {
		Record record;
		record = super.getRecord(key);
		if (record != null) {
			return (ThreadInfoRecord) record;
		}
		return null;
	}

}
