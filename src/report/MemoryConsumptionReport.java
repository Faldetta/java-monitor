package report;

import keyParameters.Parameter;
import record.MemoryConsumptionRecord;
import record.Record;

/**
 * This class extend {@link Report} to model a report about memory usage.
 */
public class MemoryConsumptionReport extends Report {

	/**
	 * Construct {@link MemoryConsumptionReport}
	 */
	public MemoryConsumptionReport() {
		super();
	}

	/**
	 * Extend {@link Report#add(Record)} to force the parameter type.
	 * 
	 * @param record Try to add a {@link Record} to the internal data structure.
	 * @return {@code true} if {@code record} is correctly inserted, {@code false}
	 *         otherwise (if there is already a {@link Record} with the same key
	 *         {@link Parameter}).
	 */
	public boolean add(MemoryConsumptionRecord record) {
		return super.add(record);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public MemoryConsumptionRecord getRecord(Parameter key) {
		Record record;
		record = super.getRecord(key);
		if (record != null) {
			return (MemoryConsumptionRecord) record;
		}
		return null;
	}

}
