package report;

import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;

import analyst.Analyst;
import keyParameters.Parameter;
import record.Record;

/**
 * This abstract class model the report of an analysis made by an
 * {@link Analyst}.
 */
public abstract class Report {

    private List<Record> recordList;

    public List<Record> getRecordList() {
        return recordList;
    }

	/**
	 * Construct a {@link Report} and initialize the internal structures.
	 */
	public Report() {
		recordList = new LinkedList<Record>();
	}

	/**
	 * Try to add a {@link Record} to the internal data structure of the
	 * {@link Report} object. Two or more {@link Record} with the same key
	 * {@link Parameter} aren't allowed in the same {@link Report}.
	 * 
	 * @param record Try to add a {@link Record} to the internal data structure.
	 * @return {@code true} if the {@link Record} is correctly inserted,
	 *         {@code false} otherwise (if there is already a {@link Record} with
	 *         the same key {@link Parameter}).
	 */
	protected boolean add(Record record) {
		boolean keyAlreadyInserted = recordList.stream().anyMatch(r -> r.getKey().equals(record.getKey()));
		if (!keyAlreadyInserted) {
			recordList.add(record);
		}
		return false;
	}

	/**
	 * Return a {@link Record} by the {@link Parameter} key.
	 * 
	 * @param key Key used to retrieve a {@link Record} by the {@link Report}.
	 * @return the asked {@link Record} if present, {@code null} otherwise.
	 */
	public Record getRecord(Parameter key) {
		try {
			return recordList.stream().filter(r -> r.getKey().equals(key)).findFirst().get();
		} catch (NoSuchElementException e) {
			return null;
		}
	}

	/**
	 * Convert the {@link Report} into a string.
	 */
	@Override
	public String toString() {
		return recordList.stream().map(r -> r.toString()).reduce((s, r) -> s.concat(r)).orElse("");
	}

}
