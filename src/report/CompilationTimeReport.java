package report;

import keyParameters.Parameter;
import record.CompilationTimeRecord;
import record.Record;

/**
 * This class extend {@link Report} to model a report of a compilation time
 * analysis.
 */
public class CompilationTimeReport extends Report {

	/**
	 * Construct a {@link CompilationTimeReport}
	 */
	public CompilationTimeReport() {
		super();
	}

	/**
	 * Extend {@link Report#add(Record)} to force the parameter type.
	 * 
	 * @param record Try to add a {@link Record} to the internal data structure.
	 * @return {@code true} if {@code record} is correctly inserted, {@code false}
	 *         otherwise (if there is already a {@link Record} with the same key
	 *         {@link Parameter}).
	 */
	public boolean add(CompilationTimeRecord record) {
		return super.add(record);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CompilationTimeRecord getRecord(Parameter key) {
		Record record;
		record = super.getRecord(key);
		if (record != null) {
			return (CompilationTimeRecord) record;
		}
		return null;
	}

}
