package analyst;

import java.lang.management.CompilationMXBean;
import java.lang.management.ManagementFactory;

import keyParameters.CompilationTimeParameter;
import record.CompilationTimeRecord;
import report.CompilationTimeReport;
import report.Report;

/**
 * This class implements the {@link Analyst} interface to harvest:
 * <ul>
 * <li>time (in milliseconds) spent in compilation (JIT)</li>
 * </ul>
 */
public class CompilationTime implements Analyst {

	private CompilationMXBean compilationBean;
	private long initialCompilationTime;
	private long finalCompilationTime;

	/**
	 * Construct a {@link CompilationTime} object.
	 */
	public CompilationTime() {
		compilationBean = ManagementFactory.getCompilationMXBean();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void initialSetUp() {
		initialCompilationTime = compilationBean.getTotalCompilationTime();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void finalSetUp() {
		finalCompilationTime = compilationBean.getTotalCompilationTime();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Report getIntervalResult() {
		CompilationTimeReport report = new CompilationTimeReport();
		report.add(new CompilationTimeRecord(CompilationTimeParameter.COMPILATION_TIME_VARIATION,
				String.valueOf(finalCompilationTime - initialCompilationTime), ""));
		return report;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Report getInstantResult() {
		CompilationTimeReport report = new CompilationTimeReport();
		report.add(new CompilationTimeRecord(CompilationTimeParameter.COMPILATION_TIME,
				String.valueOf(compilationBean.getTotalCompilationTime()), ""));
		return report;
	}

}
