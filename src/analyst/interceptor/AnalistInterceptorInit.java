package analyst.interceptor;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import analyst.monitor.Monitor;
import record.TypeName;

/**
 * This interface define an annotation applicable on methods and retainable for
 * all the runtime.
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface AnalistInterceptorInit {

	/**
	 * Method defining the analysts that must be present in the required
	 * {@link Monitor}
	 * 
	 * @return a {@link TypeName} array with the required types
	 */
	TypeName[] type() default { TypeName.ALL };

	/**
	 * Method defining the name of the required {@link Monitor}
	 * 
	 * @return a {@link String} representing the monitor's name
	 */
	String name() default "Init";
}
