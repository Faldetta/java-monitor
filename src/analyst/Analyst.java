package analyst;

import analyst.monitor.Monitor;
import report.Report;

/**
 * This interface model an analyst, a module used by the {@link Monitor}. The
 * implementations of this interface target a single part of the system,
 * harvesting informations by the selected part of the system.
 */
public interface Analyst {

	/**
	 * Do the initial set up needed to harvest metrics in a selected time window,
	 * concluded by by the class implementation of {@link Analyst#finalSetUp()}.
	 */
	public void initialSetUp();

	/**
	 * Do the final set up needed to harvest metrics in a selected time window,
	 * started by the class implementation of {@link Analyst#initialSetUp()}.
	 */
	public void finalSetUp();

	/**
	 * Return a {@link Report} containing the metrics harvested in the time window
	 * delimited by the class implementation of {@link Analyst#initialSetUp()} and
	 * {@link Analyst#finalSetUp()}.
	 * 
	 * @return a {@link Report} containing the harvested metrics.
	 */
	public Report getIntervalResult();

	/**
	 * Return a {@link Report} containing the metrics harvested instantly when this
	 * method is called.
	 * 
	 * @return {@link Report} containing the harvested metrics.
	 */
	public Report getInstantResult();

}
