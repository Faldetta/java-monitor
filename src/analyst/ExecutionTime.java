package analyst;

import keyParameters.ExecutionTimeParameter;
import record.ExecutionTimeRecord;
import report.ExecutionTimeReport;
import report.Report;

/**
 * This class implements the {@link Analyst} interface to harvest:
 * <ul>
 * <li>execution time (in milliseconds)</li>
 * </ul>
 */
public class ExecutionTime implements Analyst {

	private long startTime;
	private long stopTime;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void initialSetUp() {
		this.startTime = System.currentTimeMillis();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void finalSetUp() {
		this.stopTime = System.currentTimeMillis();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Report getIntervalResult() {
		ExecutionTimeReport report = new ExecutionTimeReport();
		report.add(new ExecutionTimeRecord(ExecutionTimeParameter.INTERVAL_EXECUTION_TIME,
				String.valueOf(stopTime - startTime), ""));
		return report;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Report getInstantResult() {
		ExecutionTimeReport report = new ExecutionTimeReport();
		report.add(new ExecutionTimeRecord(ExecutionTimeParameter.TOTAL_EXECUTION_TIME,
				String.valueOf(System.currentTimeMillis()), ""));
		return report;
	}

}
