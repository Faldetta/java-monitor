/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author daniele
 */
package analyst;

import analyst.interceptor.AnalistInterceptorFinalSetup;
import analyst.interceptor.AnalistInterceptorInit;
import java.util.LinkedList;
import java.util.List;
import record.TypeName;

public class TestAnnotation {

    @AnalistInterceptorInit(type = {TypeName.CPU,TypeName.EXECUTION_TIME})
    @AnalistInterceptorFinalSetup
    public void mesure() throws InterruptedException {
        List<Integer> list = new LinkedList<Integer>();
        for (int i = 0; i < 100000; i++) {
            list.add(Integer.valueOf(i));
        }

        Thread.sleep(5000);
    }
}
