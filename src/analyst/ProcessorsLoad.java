package analyst;

import java.lang.management.ManagementFactory;
import java.lang.management.OperatingSystemMXBean;

import keyParameters.ProcessorsLoadParameter;
import record.ProcessorsLoadRecord;
import report.ProcessorsLoadReport;
import report.Report;

/**
 * This class implements the {@link Analyst} interface to harvest:
 * <ul>
 * <li>process CPU load [0,1]</li>
 * <li>system CPU load [0,1]</li>
 * <li>time spent by the CPU on the current process (nanoseconds)</li>
 * </ul>
 * if possible, otherwise:
 * <ul>
 * <li>system CPU average load [0,1]</li>
 * </ul>
 */
public class ProcessorsLoad implements Analyst {

	private OperatingSystemMXBean OSBean;
	private com.sun.management.OperatingSystemMXBean castedBean;
	private double initialProcessCPULoad;
	private double finalProcessCPULoad;
	private double initialSystemCPULoad;
	private double finalSystemCPULoad;
	private long initialProcessCPUTime;
	private long finalProcessCPUTime;
	private double initialLoadAverage;
	private double finalLoadAverage;
	private boolean isBeanFromComSunManagement;

	/**
	 * Construct a {@link ProcessorsLoad} object.
	 */
	public ProcessorsLoad() {
		OperatingSystemMXBean MayBean = ManagementFactory.getOperatingSystemMXBean();
		if (MayBean instanceof com.sun.management.OperatingSystemMXBean) {
			isBeanFromComSunManagement = true;
			castedBean = (com.sun.management.OperatingSystemMXBean) MayBean;
		} else {
			isBeanFromComSunManagement = false;
			OSBean = MayBean;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void initialSetUp() {
		if (isBeanFromComSunManagement) {
			initialProcessCPULoad = castedBean.getProcessCpuLoad();
			initialSystemCPULoad = castedBean.getSystemCpuLoad();
			initialProcessCPUTime = castedBean.getProcessCpuTime();
		} else {
			initialLoadAverage = OSBean.getSystemLoadAverage();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void finalSetUp() {
		if (isBeanFromComSunManagement) {
			finalProcessCPULoad = castedBean.getProcessCpuLoad();
			finalSystemCPULoad = castedBean.getSystemCpuLoad();
			finalProcessCPUTime = castedBean.getProcessCpuTime();
		} else {
			finalLoadAverage = OSBean.getSystemLoadAverage();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Report getIntervalResult() {
		ProcessorsLoadReport report = new ProcessorsLoadReport();
		if (isBeanFromComSunManagement) {
			report.add(new ProcessorsLoadRecord(ProcessorsLoadParameter.PROCESS_LOAD_VARIATION,
					String.valueOf(finalProcessCPULoad - initialProcessCPULoad), ""));
			report.add(new ProcessorsLoadRecord(ProcessorsLoadParameter.SYSTEM_LOAD_VARIATION,
					String.valueOf(finalSystemCPULoad - initialSystemCPULoad), ""));
			report.add(new ProcessorsLoadRecord(ProcessorsLoadParameter.PROCESS_TIME_VARIATION,
					String.valueOf(finalProcessCPUTime - initialProcessCPUTime), ""));
		} else {
			report.add(new ProcessorsLoadRecord(ProcessorsLoadParameter.SYSTEM_LOAD_AVERAGE_VARIATION,
					String.valueOf((finalLoadAverage - initialLoadAverage)), ""));
		}
		return report;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Report getInstantResult() {
		ProcessorsLoadReport report = new ProcessorsLoadReport();
		if (isBeanFromComSunManagement) {
			report.add(new ProcessorsLoadRecord(ProcessorsLoadParameter.PROCESS_LOAD,
					String.valueOf(castedBean.getProcessCpuLoad()), ""));
			report.add(new ProcessorsLoadRecord(ProcessorsLoadParameter.SYSTEM_LOAD,
					String.valueOf(castedBean.getSystemCpuLoad()), ""));
			report.add(new ProcessorsLoadRecord(ProcessorsLoadParameter.PROCESS_TIME,
					String.valueOf(castedBean.getProcessCpuTime()), ""));
		} else {
			report.add(new ProcessorsLoadRecord(ProcessorsLoadParameter.SYSTEM_LOAD_AVERAGE,
					String.valueOf(OSBean.getSystemLoadAverage()), ""));
		}
		return report;
	}

}
