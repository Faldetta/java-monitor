package analyst;

import java.lang.management.ClassLoadingMXBean;
import java.lang.management.ManagementFactory;

import keyParameters.LoadedClassesParameter;
import record.LoadedClassesRecord;
import report.LoadedClassesReport;
import report.Report;

/**
 * This class implements the {@link Analyst} interface to harvest:
 * <ul>
 * <li>number of loaded classes</li>
 * <li>number of unloaded classes</li>
 * </ul>
 */
public class LoadedClasses implements Analyst {

	private ClassLoadingMXBean classesBean;
	private long initialLoadedClasses;
	private long initialUnloadedClasses;
	private long finalLoadedClasses;
	private long finalUnloadedClasses;

	/**
	 * Construct a {@link LoadedClasses} object.
	 */
	public LoadedClasses() {
		classesBean = ManagementFactory.getClassLoadingMXBean();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void initialSetUp() {
		initialLoadedClasses = classesBean.getTotalLoadedClassCount();
		initialUnloadedClasses = classesBean.getUnloadedClassCount();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void finalSetUp() {
		finalLoadedClasses = classesBean.getTotalLoadedClassCount();
		finalUnloadedClasses = classesBean.getUnloadedClassCount();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Report getIntervalResult() {
		LoadedClassesReport report = new LoadedClassesReport();
		report.add(new LoadedClassesRecord(LoadedClassesParameter.LOADED_CLASSES_VARIATION,
				String.valueOf(finalLoadedClasses - initialLoadedClasses), ""));
		report.add(new LoadedClassesRecord(LoadedClassesParameter.UNLOADED_CLASSES_VARIATION,
				String.valueOf(finalUnloadedClasses - initialUnloadedClasses), ""));
		return report;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Report getInstantResult() {
		LoadedClassesReport report = new LoadedClassesReport();
		report.add(new LoadedClassesRecord(LoadedClassesParameter.LOADED_CLASSES,
				String.valueOf(classesBean.getLoadedClassCount()), ""));
		report.add(new LoadedClassesRecord(LoadedClassesParameter.UNLOADED_CLASSES,
				String.valueOf(classesBean.getUnloadedClassCount()), ""));
		return report;
	}

}
