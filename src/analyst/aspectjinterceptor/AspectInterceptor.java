package analyst.aspectjinterceptor;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import analyst.interceptor.AnalistInterceptorFinalSetup;
import analyst.interceptor.AnalistInterceptorInit;
import analyst.monitor.Monitor;
import analyst.monitor.MonitorFactory;
import record.TypeName;

/**
 * This class define the implementations of the annotations defined in the
 * analyst.interceptor package.
 */
@Aspect
@Component
public class AspectInterceptor {

	/**
	 * This method implement the behavior to be implemented when the
	 * {@link AnalistInterceptorInit} annotation is called, before the method
	 * execution
	 * 
	 * @param joinPoint              point in which inject the defined annotation
	 * @param analistInterceptorInit implementing annotation, used for inputs to the
	 *                               implementation
	 */
	@Before("@annotation(analistInterceptorInit) && execution(* *(..))")
	public void startMonitor(JoinPoint joinPoint, analyst.interceptor.AnalistInterceptorInit analistInterceptorInit) {
		TypeName[] type = analistInterceptorInit.type();
		String typeAsString = "";
		for (int i = 0; i < type.length; i++) {
			typeAsString = typeAsString + " " + type[i].getValue();
		}
		String name = analistInterceptorInit.name();
		System.out.println("Variabile name valore:" + type);

		Monitor m = MonitorFactory.of(Thread.currentThread().getId(), type);
		System.out.println("Start to monitor on  thead" + Thread.currentThread().getId() + "for joint point" + joinPoint
				+ " with label: " + name + " with type: " + typeAsString);
		m.initialSetUp();

	}

	/**
	 * This is used by After annotation to define the entry point to define the
	 * implementation of the {@link AnalistInterceptorFinalSetup} annotation.
	 */
	@Pointcut("@annotation(analyst.interceptor.AnalistInterceptorFinalSetup) && execution(* *(..))")
	public void defineEntryPointEnd() {
	}

	/**
	 * This method implement the behavior to be implemented when the
	 * {@link AnalistInterceptorFinalSetup} annotation is called, after the method
	 * execution
	 * 
	 * @param joinPoint point in which inject the defined annotation
	 */
	@After("defineEntryPointEnd()")
	public void endMonitor(JoinPoint joinPoint) {
		Monitor m = MonitorFactory.of(Thread.currentThread().getId(), null);
		System.out.println(
				"Finish to monitor on  thead" + Thread.currentThread().getId() + "for joint point" + joinPoint);
		m.finalSetUp();
		m.printIntervalResults();
		m.printInstantResults();
	}
}
