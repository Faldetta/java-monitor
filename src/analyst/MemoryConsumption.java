package analyst;

import keyParameters.MemoryParameter;
import record.MemoryConsumptionRecord;
import report.MemoryConsumptionReport;
import report.Report;

/**
 * This class implements the {@link Analyst} interface to harvest:
 * <ul>
 * <li>memory usage (in bytes)</li>
 * </ul>
 */
public class MemoryConsumption implements Analyst {

	private static final long MEGABYTE = 1024L * 1024L;
	private long initialMemory;
	private long finalMemory;

	/**
	 * Convert the value given as input from B to MB
	 * 
	 * @param bytes Bytes amount to convert to MB
	 * @return the amount of MB
	 */
	public static long bytesToMegabytes(long bytes) {
		return bytes / MEGABYTE;
	}

	/**
	 * Ask the {@link Runtime} the amount of used memory
	 * 
	 * @return the used memory in Bytes
	 */
	private long getMemoryUsage() {
		Runtime runtime = Runtime.getRuntime();
		runtime.gc();
		return runtime.totalMemory() - runtime.freeMemory();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void initialSetUp() {
		initialMemory = getMemoryUsage();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void finalSetUp() {
		finalMemory = getMemoryUsage();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Report getIntervalResult() {
		MemoryConsumptionReport report = new MemoryConsumptionReport();
		report.add(new MemoryConsumptionRecord(MemoryParameter.MEMORY_VARIATION,
				String.valueOf(finalMemory - initialMemory), ""));
		return report;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Report getInstantResult() {
		MemoryConsumptionReport report = new MemoryConsumptionReport();
		report.add(new MemoryConsumptionRecord(MemoryParameter.MEMORY, String.valueOf(getMemoryUsage()), ""));
		return report;
	}

}
