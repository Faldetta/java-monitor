package analyst.monitor;

import analyst.Analyst;
import analyst.CompilationTime;
import analyst.ExecutionTime;
import analyst.LoadedClasses;
import analyst.MemoryConsumption;
import analyst.ProcessorsLoad;
import analyst.ThreadInfo;
import java.util.ArrayList;
import java.util.List;

/**
 * Component responsible for the aggregation of {@link Analyst} to monitor
 * interesting aspects of the system.
 */
public class Monitor {

	private long threadID;
	private List<Analyst> analysts;

	/**
	 * Construct a new {@link Monitor}.
	 * 
	 * @param threadID ID of the thread (because can be used by the
	 *                 {@link ThreadInfo} analyst)
	 */
	public Monitor(long threadID) {
		analysts = new ArrayList<Analyst>();
//		analysts.add(new CompilationTime());
//		analysts.add(new ExecutionTime());
//		analysts.add(new LoadedClasses());
//		analysts.add(new MemoryConsumption());
//		try {
//			analysts.add(new ProcessorsLoad());
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		analysts.add(new ThreadInfo(threadID));
	}

	/**
	 * Call {@link Analyst#initialSetUp()} on all the {@link Analyst} in the
	 * {@link Monitor}.
	 */
	public void initialSetUp() {
		write("analyst.monitor.Monitor.initialSetUp()");
		analysts.stream().forEach(a -> a.initialSetUp());
	}

	/**
	 * Call {@link Analyst#finalSetUp()} on all the {@link Analyst} in the
	 * {@link Monitor}.
	 */
	public void finalSetUp() {
		write("analyst.monitor.Monitor.finalSetUp()");
		analysts.stream().forEach(a -> a.finalSetUp());
	}

	/**
	 * Print the interval results of all the {@link Analyst} in the {@link Monitor}.
	 */
	public void printIntervalResults() {
		write("analyst.monitor.Monitor.printIntervalResult1s()");
		analysts.stream().forEach(a -> write(a.getIntervalResult().toString()));
	}

	/**
	 * Print the instant results of all the {@link Analyst} in the {@link Monitor}.
	 */
	public void printInstantResults() {
		write("analyst.monitor.Monitor.printInstantResults()");
		analysts.stream().forEach(a -> write(a.getInstantResult().toString()));
	}

	/**
	 * Add a given {@link Analyst} to the {@link Monitor}.
	 * 
	 * @param analyst {@link Analyst} to add to the {@link Monitor}.
	 */
	public void addAnalyst(Analyst analyst) {
		analysts.add(analyst);
	}

	/**
	 * Method to unify the output method of the monitor. This unification permit a
	 * simple change of the output method.
	 * 
	 * @param string {@link String} used to generate output.
	 */
	public void write(String string) {
		System.out.println(string);
	}

	public long getThreadID() {
		return threadID;
	}

	public List<Analyst> getAnalysts() {
		return analysts;
	}

}
