/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package analyst.monitor;

import java.util.HashMap;
import java.util.Map;

import analyst.Analyst;
import analyst.CompilationTime;
import analyst.ExecutionTime;
import analyst.LoadedClasses;
import analyst.MemoryConsumption;
import analyst.ProcessorsLoad;
import analyst.ThreadInfo;
import record.TypeName;

/**
 * Factory class to generate {@link Monitor} singletons.
 */
public class MonitorFactory {

	private static final Map<Long, Monitor> MONITOR_MAP = new HashMap<>();

	public static Map<Long, Monitor> getMONITOR_MAP() {
		return MONITOR_MAP;
	}

	/**
	 * Generate a {@link Monitor} with a given thread ID, if isn't generated yet,
	 * otherwise return the requested {@link Monitor} instance.
	 * 
	 * @param threadID ID of the thread, used to generate a new {@link Monitor} or
	 *                 to retrieve an existing one.
	 * @param types    Array of {@link TypeName} used to generate a {@link Monitor}
	 *                 with the right set of {@link Analyst}.
	 * @return The wanted {@link Monitor}.
	 */
	public static Monitor of(long threadID, TypeName[] types) {
		if (MonitorFactory.MONITOR_MAP.get(threadID) == null) {
			Monitor m = newMonitor(threadID, types);
			MonitorFactory.MONITOR_MAP.put(threadID, m);
			return m;
		} else {
			return MonitorFactory.MONITOR_MAP.get(threadID);
		}
	}

	protected static Monitor newMonitor(long threadID, TypeName[] types) {
		Monitor monitor = new Monitor(threadID);
		if (types != null) {
			for (TypeName type : types) {
				switch (type) {
				case ALL:
					complete(threadID, monitor);
					break;
				case CPU:
					monitor.addAnalyst(new ProcessorsLoad());
					break;
				case RAM:
					monitor.addAnalyst(new MemoryConsumption());
					break;
				case LOADED_CLASS:
					monitor.addAnalyst(new LoadedClasses());
					break;
				case EXECUTION_TIME:
					monitor.addAnalyst(new ExecutionTime());
					break;
				default:
					throw new AssertionError(type.name());
				}

			}
		} else {
			complete(threadID, monitor);
		}
		return monitor;
	}

	private static void complete(long threadID, Monitor monitor) {
		monitor.addAnalyst(new CompilationTime());
		monitor.addAnalyst(new ExecutionTime());
		monitor.addAnalyst(new LoadedClasses());
		monitor.addAnalyst(new MemoryConsumption());
		monitor.addAnalyst(new ProcessorsLoad());
		monitor.addAnalyst(new ThreadInfo(threadID));
	}

}
