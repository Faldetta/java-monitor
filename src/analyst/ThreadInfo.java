package analyst;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;

import keyParameters.ThreadInfoParameter;
import record.ThreadInfoRecord;
import report.Report;
import report.ThreadInfoReport;

/**
 * This class implements the {@link Analyst} interface to harvest:
 * <ul>
 * <li>the number of threads</li>
 * </ul>
 * and if is possible:
 * <ul>
 * <li>time spent by the thread in CPU mode (nanoseconds)</li>
 * <li>time spent by the thread in User mode (nanoseconds)</li>
 * </ul>
 */
public class ThreadInfo implements Analyst {

	private ThreadMXBean threadBean;
	private long initialThreadCount;
	private long finalThreadCount;
	private long initialThreadCpuTime;
	private long finalThreadCpuTime;
	private long initialThreadUserTime;
	private long finalThreadUserTime;
	private long threadID;
	private boolean canMonitorCpuTime;

	/**
	 * Construct a {@link ThreadInfo} object.
	 * 
	 * @param threadID ID of the thread to analyze.
	 */
	public ThreadInfo(long threadID) {
		threadBean = ManagementFactory.getThreadMXBean();
		this.threadID = threadID;
		if (threadBean.isThreadCpuTimeSupported() && threadBean.isCurrentThreadCpuTimeSupported()) {
			canMonitorCpuTime = true;
			threadBean.setThreadCpuTimeEnabled(canMonitorCpuTime);
		} else {
			canMonitorCpuTime = false;
			threadBean.setThreadCpuTimeEnabled(canMonitorCpuTime);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void initialSetUp() {
		initialThreadCount = threadBean.getTotalStartedThreadCount();
		if (canMonitorCpuTime) {
			initialThreadCpuTime = threadBean.getThreadCpuTime(threadID);
			initialThreadUserTime = threadBean.getThreadUserTime(threadID);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void finalSetUp() {
		finalThreadCount = threadBean.getTotalStartedThreadCount();
		if (canMonitorCpuTime) {
			finalThreadCpuTime = threadBean.getThreadCpuTime(threadID);
			finalThreadUserTime = threadBean.getThreadUserTime(threadID);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Report getIntervalResult() {
		ThreadInfoReport report = new ThreadInfoReport();
		report.add(new ThreadInfoRecord(ThreadInfoParameter.THREAD_COUNT_VARIATION,
				String.valueOf(finalThreadCount - initialThreadCount), ""));
		if (canMonitorCpuTime) {
			report.add(new ThreadInfoRecord(ThreadInfoParameter.THREAD_CPU_TIME_VARIATION,
					String.valueOf(finalThreadCpuTime - initialThreadCpuTime), ""));
			report.add(new ThreadInfoRecord(ThreadInfoParameter.THREAD_USER_TIME_VARIATION,
					String.valueOf(finalThreadUserTime - initialThreadUserTime), ""));
		}
		return report;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Report getInstantResult() {
		ThreadInfoReport report = new ThreadInfoReport();
		report.add(new ThreadInfoRecord(ThreadInfoParameter.THREAD_COUNT,
				String.valueOf(threadBean.getTotalStartedThreadCount()), ""));
		if (canMonitorCpuTime) {
			report.add(new ThreadInfoRecord(ThreadInfoParameter.THREAD_CPU_TIME,
					String.valueOf(threadBean.getThreadCpuTime(threadID)), ""));
			report.add(new ThreadInfoRecord(ThreadInfoParameter.THREAD_USER_TIME,
					String.valueOf(threadBean.getThreadUserTime(threadID)), ""));
		}
		return report;
	}

	/**
	 * Tell if the CPU time analysis is supported
	 * 
	 * @return {@code true} if CPU time analysis is supported, {@code false}
	 *         otherwise.
	 */
	public boolean canMonitorCpuTime() {
		return canMonitorCpuTime;
	}

}
