/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package record;

import analyst.Analyst;
import analyst.monitor.Monitor;

/**
 * {@code Enum} class used to assign one or more {@link Analyst} to a
 * {@link Monitor}.
 */
public enum TypeName {
	/**
	 * Key to generate a monitor using all the defined analysts
	 */
	ALL("all"), 
	
	/**
	 * Key to generate a monitor using the CPU analysts
	 */
	CPU("cpu"), 
	
	/**
	 * Key to generate a monitor using the RAM analysts
	 */
	RAM("ram"), 
	
	/**
	 * Key to generate a monitor using the loaded classes analysts
	 */
	LOADED_CLASS("loaded_class"), 
	
	/**
	 * Key to generate a monitor using the execution time analysts
	 */
	EXECUTION_TIME("executionTime");

	// declaring private variable for getting values
	private String value;

	/**
	 * Getter for the {@link String} representation of an {@code Enum} value
	 * 
	 * @return A string representing the value of the {@code Enum}
	 */
	public String getValue() {
		return this.value;
	}

	// enum constructor - cannot be public or protected
	private TypeName(String value) {
		this.value = value;
	}

}
