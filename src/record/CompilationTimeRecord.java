package record;

import keyParameters.CompilationTimeParameter;

/**
 * This class extends a {@link Record} to contain data about compilation time.
 */
public class CompilationTimeRecord extends Record {

	/**
	 * Construct a {@link Record} with the parameters received in input.
	 * 
	 * @param key         Key identifying the meaning of the {@code value}
	 *                    parameter.
	 * @param value       {@link String} containing a value with the meaning defined
	 *                    by the {@code key} parameter.
	 * @param probingData Additional data that can be acquired about the context in
	 *                    which the data are harvested.
	 */
	public CompilationTimeRecord(CompilationTimeParameter key, String value, String probingData) {
		super(key, value, probingData);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CompilationTimeParameter getKey() {
		return (CompilationTimeParameter) super.getKey();
	}

}
