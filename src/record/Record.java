package record;

import keyParameters.Parameter;

/**
 * This abstract class model a record, a triple defined by a key
 * {@link Parameter}, a {@link String} value, and another {@link String} that
 * can contain additional informations about the situation in which the value is
 * probed.
 */
public abstract class Record {

	private Parameter key;
	private String value;
	private String probingData;

	/**
	 * Construct a {@link Record} with the parameters received in input.
	 * 
	 * @param key         Key identifying the meaning of the {@code value}
	 *                    parameter.
	 * @param value       {@link String} containing a value with the meaning defined
	 *                    by the {@code key} parameter.
	 * @param probingData Additional data that can be acquired about the context in
	 *                    which the data are harvested.
	 */
	public Record(Parameter key, String value, String probingData) {
		this.key = key;
		this.value = value;
		this.probingData = probingData;
	}

	/**
	 * Getter for the {@code key} of the {@link Record}
	 * 
	 * @return The key of the {@link Record}.
	 */
	public Parameter getKey() {
		return key;
	}

	/**
	 * Getter for the {@code value} of the {@link Record}
	 * 
	 * @return The value of the {@link Record}.
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Getter for the {@code probingData} of the {@link Record}
	 * 
	 * @return The probing data of the {@link Record}.
	 */
	public String getProbingData() {
		return probingData;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Record) {
			Record other = (Record) obj;
			if (this.key.equals(other.key) && this.value.equals(other.value)
					&& this.probingData.contentEquals(other.probingData)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return probingData + "    " + key.toString() + ": " + value + "\n";
	}

}
