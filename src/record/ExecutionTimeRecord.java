package record;

import keyParameters.ExecutionTimeParameter;

/**
 * This class extends a {@link Record} to contain data about execution time.
 */
public class ExecutionTimeRecord extends Record {

	/**
	 * Construct a {@link Record} with the parameters received in input.
	 * 
	 * @param key         Key identifying the meaning of the {@code value}
	 *                    parameter.
	 * @param value       {@link String} containing a value with the meaning defined
	 *                    by the {@code key} parameter.
	 * @param probingData Additional data that can be acquired about the context in
	 *                    which the data are harvested.
	 */
	public ExecutionTimeRecord(ExecutionTimeParameter key, String value, String probingData) {
		super(key, value, probingData);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ExecutionTimeParameter getKey() {
		return (ExecutionTimeParameter) super.getKey();
	}

}
