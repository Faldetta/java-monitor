package record;

import keyParameters.MemoryParameter;

/**
 * This class extends a {@link Record} to contain data about the memory
 * consumption.
 */
public class MemoryConsumptionRecord extends Record {

	/**
	 * Construct a {@link Record} with the parameters received in input.
	 * 
	 * @param key         Key identifying the meaning of the {@code value}
	 *                    parameter.
	 * @param value       {@link String} containing a value with the meaning defined
	 *                    by the {@code key} parameter.
	 * @param probingData Additional data that can be acquired about the context in
	 *                    which the data are harvested.
	 */
	public MemoryConsumptionRecord(MemoryParameter key, String value, String probingData) {
		super(key, value, probingData);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public MemoryParameter getKey() {
		return (MemoryParameter) super.getKey();
	}

}
